#include <iostream>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>
#include "fcgio.h"
#include <hiredis.h>
#include <mysql.h>

using namespace std;
using std::string;

const char* REDIS_SERVER    = "127.0.0.1";
const int   REDIS_PORT      = 6379;
const char* AURORA_ENDPOINT = "s871aurd01aw.cluster-c9gfi8ng5yz4.us-east-1.rds.amazonaws.com";
const char* AURORA_USERNAME = "shqadmin";
const char* AURORA_PASSWORD = "TODO";
const char* AURORA_DB       = "shqdefault";

const char* const g_loiidPrefix = "/loiid/";
const int g_loiidPrefixLen = strlen(g_loiidPrefix); 
MYSQL* g_aurora = NULL;
redisContext* g_redis = NULL;

void verySimpleLog(const char* msg) {
    FILE *f = fopen("log.txt", "a+");
    fprintf(f, "%s\n", msg);
    fclose(f);
}

void openRedis() {
    g_redis = redisConnect(REDIS_SERVER, REDIS_PORT);
    if (g_redis == NULL || g_redis->err) {
        if (g_redis) {
            string msg ("Redis error:  ");
            msg += g_redis->errstr;
            verySimpleLog(msg.c_str());
        }
        else {
            verySimpleLog("Can't allocate redis context");
        }
    } 
    else {
        verySimpleLog("Opened Redis");
    }
}

void redisGet(string& res, const char* key) {
    res.clear();
    if (!g_redis) return;
    string cmd ("GET ");
    cmd += key;
    redisReply* reply = static_cast<redisReply*> (redisCommand(g_redis, cmd.c_str()));
    if (reply) {
        if (reply->type == REDIS_REPLY_NIL) {
            // leave 'res' empty.
        }
        else if (reply->type == REDIS_REPLY_STRING && reply->str != NULL) {
            res = reply->str;
        }
        freeReplyObject(reply);
    }
    else {
        // Haven't actually seen this.
        verySimpleLog("getRedisString no reply.");
    }
}

void redisSet(const char* key, const char* value) {
    if (!g_redis) return;
    string cmd ("SET ");
    cmd += key;
    cmd += " ";
    cmd += value;
    redisCommand(g_redis, cmd.c_str());
}

void redisSetList(const char* key, const std::vector<string>& values) {
    if (!g_redis) return;
    string redisCmd;
    redisCmd += "DEL ";
    redisCmd += key;
    redisCommand(g_redis, redisCmd.c_str());
    int len = values.size();
    if (len > 0) {
        redisCmd.clear();
        redisCmd += "LPUSH ";
        redisCmd += key;
        for (int i=0; i<len; ++i) {
            redisCmd += " ";
            redisCmd += values[i];
        }
        redisCommand(g_redis, redisCmd.c_str());
    }
}

void redisGetList(const char* key, std::vector<string>& values) {
    values.clear();
    if (!g_redis) return;
    string redisCmd;
    redisCmd += "LRANGE ";
    redisCmd += key;
    redisCmd += " 0 10000";
    redisCommand(g_redis, redisCmd.c_str());
    redisReply* reply = static_cast<redisReply*> (redisCommand(g_redis, redisCmd.c_str()));
    if (reply) {
        if (reply->type == REDIS_REPLY_ARRAY) {
            int ct = reply->elements;
            for (int i=0; i<ct; ++i) {
                redisReply* elem = static_cast<redisReply*> (reply->element[i]);
                if (elem->type == REDIS_REPLY_STRING && elem->str != NULL) {
                    values.push_back(string(elem->str));
                }
            }
        }
        // This also frees the inner objects.
        freeReplyObject(reply);
    }
}

void logAuroraError() {
    verySimpleLog(mysql_error(g_aurora));
}

void openAurora() {
    g_aurora = mysql_init(NULL);
    if (mysql_real_connect(g_aurora, 
         AURORA_ENDPOINT,
         AURORA_USERNAME, AURORA_PASSWORD,
         AURORA_DB, 0, NULL, 0) == NULL) {
        logAuroraError();
        // Explicitly set to null; otherwise we'll crash.
        g_aurora = NULL;
    }
    else {
        verySimpleLog("Opened Aurora");
    }
}

void getSegmentsFromAurora(const std::string& loiid, std::vector<string>& segs) {
    segs.clear();
    if (!g_aurora) return;

    // TODO:  Safe SQL parameterization
    char str[1024];
    sprintf(str, " \
SELECT s.id,s.name FROM segment s \
JOIN segment_loiid sl \
ON s.id = sl.segment_id \
WHERE sl.loiid = '%s' \
    ", loiid.c_str());

    if (mysql_query(g_aurora, str)) {
        logAuroraError();
    }
    MYSQL_RES *result = mysql_store_result(g_aurora);
    if (result == NULL) {
        logAuroraError();
    }
    MYSQL_ROW row;
    while ((row = mysql_fetch_row(result))) {
        sprintf(str, "<%s> <%s>", row[0], row[1]);
        verySimpleLog(str);
        segs.push_back(string(row[1]));
    }  
    mysql_free_result(result);
    verySimpleLog("did query");  
}

int main(void) {
    verySimpleLog("Starting fastcgi server...");
    verySimpleLog(mysql_get_client_info());

    // Backup the stdio streambufs
    streambuf * cin_streambuf  = cin.rdbuf();
    streambuf * cout_streambuf = cout.rdbuf();
    streambuf * cerr_streambuf = cerr.rdbuf();

    FCGX_Request request;

    FCGX_Init();
    FCGX_InitRequest(&request, 0, 0);

    openAurora();
    openRedis();

    int launches = 0;

    // Located outside loop to minimize memory reallocs.
    std::vector<string> segs;

    while (FCGX_Accept_r(&request) == 0) {

        fcgi_streambuf cin_fcgi_streambuf(request.in);
        fcgi_streambuf cout_fcgi_streambuf(request.out);
        fcgi_streambuf cerr_fcgi_streambuf(request.err);

        cin.rdbuf(&cin_fcgi_streambuf);
        cout.rdbuf(&cout_fcgi_streambuf);
        cerr.rdbuf(&cerr_fcgi_streambuf);

        launches++;
        char msg[1024];
        // getenv() doesn't return anything.
        const char *uri = FCGX_GetParam("REQUEST_URI", request.envp);
        if (!uri) uri = "";

        // TODO:  very crude URI parsing.
        string uriStr (uri);

        string segsJson;
        segsJson += "[";
        if (uriStr.length() > g_loiidPrefixLen && uriStr.compare(0, g_loiidPrefixLen, g_loiidPrefix) == 0) {
            string loiid (uriStr.substr(g_loiidPrefixLen));
            verySimpleLog(loiid.c_str());
            redisGetList(loiid.c_str(), segs);
            int cachedLen = segs.size();
            if (cachedLen == 0) {
                string msg;
                msg += "Redis cache miss for ";
                msg += loiid;
                verySimpleLog(msg.c_str());
                getSegmentsFromAurora(loiid, segs);
                redisSetList(loiid.c_str(), segs);
            }

            int retLen = segs.size();
            for (int i=0; i<retLen; ++i) {
                if (i > 0) {
                    segsJson += ",";
                }
                segsJson += "\"";
                segsJson += segs[i];
                segsJson += "\"";
            }
        }
        segsJson += "]";

        string redisRes;
        redisGet(redisRes, "foobar");
        verySimpleLog(redisRes.c_str());
        redisSet("foobar", "123");

        sprintf(msg, "{\"launches\":%d, \"pid\":%d, \"uri\":\"%s\"", launches, getpid(), uri);

        string resJson;
        resJson += msg;
        resJson += ",\"segs\":";
        resJson += segsJson;
        resJson += "}";
        cout << "application/json\r\n\r\n";
        cout << resJson.c_str() << "\n";

        // Note: the fcgi_streambuf destructor will auto flush
    }

    if (g_aurora) {
        mysql_close(g_aurora);
        g_aurora = NULL;
    }
    if (g_redis) {
        redisFree(g_redis);
        g_redis = NULL;
    }

    // restore stdio streambufs
    cin.rdbuf(cin_streambuf);
    cout.rdbuf(cout_streambuf);
    cerr.rdbuf(cerr_streambuf);

    return 0;
}

