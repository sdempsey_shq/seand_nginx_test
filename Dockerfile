FROM ubuntu:14.04
RUN \
  apt-get update && \
  apt-get install -y man vim libfcgi-dev  spawn-fcgi nginx curl \
                     libhiredis-dev libmysqlclient-dev telnet gcc g++ make redis-server 

ADD . /opt/app

WORKDIR /opt/app
# Since redis-server blocks; launching as -d will persist.
# -F 1 : number of fastcgi handlers to fork.
CMD \
  make ;\
  spawn-fcgi -p 8000 -F 1 test_fastcgi ;\
  nginx -c /opt/app/conf/nginx.conf ;\
  redis-server

