#! /bin/bash

docker kill $(docker ps -q --filter ancestor=smarterhq/seand_nginx_test)
docker build -t smarterhq/seand_nginx_test .
docker run -d -v /:/cont -p 8081:80 smarterhq/seand_nginx_test
docker logs -f $(docker ps -q --filter ancestor=smarterhq/seand_nginx_test)

